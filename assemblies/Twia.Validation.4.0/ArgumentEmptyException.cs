﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;
using Twia.Validation.Resources;

namespace Twia.Validation
{
    /// <summary>
    /// A <see cref="string"/> argument to a method or the value on a string property setter was an <see cref="String.Empty"/> string.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The name of the parameter that caused the <see cref="ArgumentEmptyException"/> to be thrown can be retrieved through the <see cref="ArgumentException.ParamName"/> property.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed class ArgumentEmptyException : ArgumentException
    {
        private const string _paramNameName = "paramName";
        private const string _messageName = "message";

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentEmptyException"/> class with a given offending parameter name in <paramref name="paramName"/>.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused this exception. Must not be <see langword="null"/> or <see cref="String.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="paramName"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="paramName"/> is <see cref="String.Empty"/>.</exception>
        public ArgumentEmptyException([NotNull] string paramName)
            : base(Messages.MessageArgumentEmpty, paramName)
        {
            Validators.ValidateNotNull(paramName, _paramNameName);
            Validators.ValidateNotEmpty(paramName, _paramNameName);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentEmptyException"/> class with a given offending parameter name in <paramref name="paramName"/> and a custom message in <paramref name="message"/>.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused this exception. Must not be <see langword="null"/> or <see cref="String.Empty"/>.</param>
        /// <param name="message">A custom message that describes the error. Must not be <see langword="null"/> or <see cref="String.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="paramName"/> is <see langword="null"/> or <paramref name="message"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="paramName"/> is <see cref="String.Empty"/> or <paramref name="message"/> is <see cref="String.Empty"/>.</exception>
        public ArgumentEmptyException([NotNull] string paramName, [NotNull] string message)
            : base(message, paramName)
        {
            Validators.ValidateNotNull(paramName, _paramNameName);
            Validators.ValidateNotEmpty(paramName, _paramNameName);
            Validators.ValidateNotNull(message, _messageName);
            Validators.ValidateNotEmpty(message, _messageName);
        } 


        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentEmptyException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        /// <remarks>
        /// This constructor is called during deserialization to reconstitute the exception object transmitted over a stream. 
        /// For more information, see <see href="http://msdn.microsoft.com/en-us/library/vstudio/90c86ass(v=vs.100).aspx">XML and SOAP Serialization</see>.
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="info"/> is <see langword="null"/>.</exception>
        private ArgumentEmptyException([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}