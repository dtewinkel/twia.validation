using System;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Twia.Validation.Resources;

namespace Twia.Validation
{
    /// <summary>
    /// Extension methods that provide common validations on string parameters for methods and on string property setters.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The parameter validation <see cref="AssertIsNotNullOrEmpty"/> can be used to validate if a parameter is not <see langword="null"/> and its value is not <see cref="string.Empty"/>.
    /// </para>
    /// <para>
    /// The parameter validations <see cref="AssertIsNotNullAndMatchesRegex(string,string,string)"/> and <see cref="AssertIsNotNullAndMatchesRegex(string,string,string,string,object[])"/> can be 
    /// used to validate if a parameter is not <see langword="null"/> and its value is matches a given regular expression.
    /// </para>
    /// <para>
    /// Also the parameter validations in <see cref="GenericsParameterValidations"/> can be used on a string, such as <see cref="GenericsParameterValidations.AssertIsNotNull{T}"/>.
    /// </para>
    /// </remarks>
    public static class StringParameterValidations
    {
        private const string _parameterNameName = "parameterName";
        private const string _regexName = "regex";
        private const string _messageName = "message";
        private const string _messageParametersName = "messageParameters";

        /// <summary>
        /// Validate if a string parameter is not <see langword="null"/> and if its value is not an <see cref="string.Empty"/> string.
        /// </summary>
        /// <param name="parameter">The string parameter to validate not to be <see langword="null"/> and its values not be <see cref="string.Empty"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <remarks>
        /// <para>
        /// This method Validates if the parameter referenced by <paramref name="parameter"/> is not <see langword="null"/> and its values is not equal to an <see cref="string.Empty"/> string. 
        /// If the value of <paramref name="parameter"/> is an <see cref="string.Empty"/> string then an <see cref="ArgumentEmptyException"/> is thrown.
        /// If <paramref name="parameter"/> is equal to <see langword="null"/> then an <see cref="ArgumentNullException"/> is thrown.
        /// </para>
        /// <para>
        /// In case of a validation fault, the value of <paramref name="parameterName"/> is set as the parameter that caused the exception in <see cref="ArgumentException.ParamName"/> in the exception. 
        /// If the parameter <paramref name="parameterName"/> causes the exception then the value of <see cref="ArgumentException.ParamName"/> will be set to "parameterName".
        /// </para>
        /// <example>
        /// <para>
        /// This example shows the use of this method on parameter 'param1'. It also shows the combination with the ReSharper <b>NotNull</b> attribute.
        /// </para>
        /// <code lang="C#">
        /// public void foo([NotNull] string param1)
        /// {
        ///     param1.AssertIsNotNullOrEmpty("param1");
        /// 
        ///     ...
        /// }
        /// </code>
        /// <para>
        /// Calling this method from the following code will result in a <see cref="ArgumentNullException"/> being thrown.
        /// </para>
        /// <code lang="C#">
        ///     string bar = null;
        ///     x.foo(bar);
        ///     // Execution will not get here, as an ArgumentNullException is thrown from the previous line.
        /// </code>
        /// </example>
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="parameter"/> is <see langword="null"/> or <paramref name="parameterName"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="parameter"/> is <see cref="string.Empty"/> or <paramref name="parameterName"/> is <see cref="string.Empty"/>.</exception>
        [AssertionMethod]
        [ContractAnnotation("parameter:null => void; parameterName:null => void")]
        public static void AssertIsNotNullOrEmpty(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this string parameter,
            [NotNull, InvokerParameterName] string parameterName)
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);

            Validators.ValidateNotNull(parameter, parameterName);
            Validators.ValidateNotEmpty(parameter, parameterName);
        }


        /// <overloads>
        /// These overloads of <b>AssertIsNotNullAndMatchesRegex</b> validate if a string parameter is not <see langword="null"/> and its <paramref name="parameter"/> matches the regular expression in <paramref name="regex"/>.
        /// </overloads>
        /// <summary>
        /// Validate if a string parameter is not <see langword="null"/> and if its value matches the regular expression in <paramref name="regex"/>.
        /// </summary>
        /// <param name="parameter">The string parameter to validate not to be <see langword="null"/> and its values to match the regular expression in <paramref name="regex"/>.</param>
        /// <param name="regex">The regular expression used to validate the string in <paramref name="parameter"/>. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <exception cref="ArgumentDoesNotMatchRegexException">
        /// <paramref name="parameter"/> does not match the regular expression in <paramref name="regex"/>.
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="parameter"/> is <see langword="null"/>, <paramref name="parameterName"/> is <see langword="null"/>, or <paramref name="regex"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="regex"/> is <see cref="string.Empty"/> or <paramref name="parameterName"/> is <see cref="string.Empty"/>.</exception>
        /// <exception cref="ArgumentException">A regular expression parsing error occurred.</exception>
        [AssertionMethod]
        [ContractAnnotation("parameter:null => void; regex:null => void; parameterName:null => void")]
        public static void AssertIsNotNullAndMatchesRegex(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this string parameter,
            [NotNull] string regex,
            [NotNull, InvokerParameterName] string parameterName)
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(regex, _regexName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);
            Validators.ValidateNotEmpty(regex, _regexName);

            Validators.ValidateNotNull(parameter, parameterName);
            Validators.ValidateMatchesRegex(parameter, regex, parameterName);
        }


        /// <summary>
        /// Asserts if  a <see cref="string"/> parameter is not <see langword="null"/> and matches the regular expression in <paramref name="regex"/>, 
        /// providing a custom <paramref name="message"/> for the <see cref="ArgumentDoesNotMatchRegexException"/>.
        /// </summary>
        /// <param name="parameter">The string parameter to validate not to be <see langword="null"/> and its values to match the regular expression in <paramref name="regex"/>.</param>
        /// <param name="regex">The regular expression used to validate the string in <paramref name="parameter"/>. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="message">
        /// The message to give in the <see cref="ArgumentDoesNotMatchRegexException"/>, if <paramref name="parameter"/> does not match the regular expression in <paramref name="regex"/>. 
        /// The message may be a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.
        /// Must not be <see langword="null"/> or <see cref="string.Empty"/>.
        /// </param>
        /// <param name="messageParameters">The message parameters, if <paramref name="message"/> is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.</param>
        /// <remarks>
        /// <para>
        /// If the message is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>, 
        /// then it can be formatted in the same way as for <see cref="string.Format(string,object[])"/>. 
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentDoesNotMatchRegexException"><paramref name="parameter"/> does not match the regular expression in <paramref name="regex"/>.</exception>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="parameter"/> is <see langword="null"/>, <paramref name="parameterName"/> is <see langword="null"/>, 
        /// <paramref name="regex"/> is <see langword="null"/>, <paramref name="message"/> is <see langword="null"/>, or
        /// <paramref name="messageParameters"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentEmptyException">
        /// <paramref name="parameterName"/> is <see cref="string.Empty"/>, <paramref name="regex"/> is <see cref="string.Empty"/>, or <paramref name="message"/> is <see cref="string.Empty"/>.
        /// </exception>
        /// <exception cref="FormatException"><paramref name="message" /> is invalid or the index of a format item is less than zero, or greater than or equal to the length of the <paramref name="messageParameters" /> array.</exception>
        /// <exception cref="ArgumentException">A regular expression parsing error occurred.</exception>
        [AssertionMethod]
        [ContractAnnotation("parameter:null => void; regex:null => void; parameterName:null => void; message:null => void; messageParameters:null => void")]
        [StringFormatMethod("message")]
        public static void AssertIsNotNullAndMatchesRegex(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this string parameter,
            [NotNull] string regex,
            [NotNull, InvokerParameterName] string parameterName,
            [NotNull] string message,
            [NotNull] params object[] messageParameters)
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(regex, _regexName);
            Validators.ValidateNotNull(message, _messageName);
            Validators.ValidateNotNull(messageParameters, _messageParametersName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);
            Validators.ValidateNotEmpty(regex, _regexName);
            Validators.ValidateNotEmpty(message, _messageName);

            Validators.ValidateNotNull(parameter, parameterName);
            Validators.ValidateMatchesRegex(parameter, regex, parameterName, message, messageParameters);
        }
    }
}