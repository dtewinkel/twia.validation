using System;
using JetBrains.Annotations;

namespace Twia.Validation
{
    /// <summary>
    /// Extension methods that provide common validations using generics.
    /// </summary>
    public static class GenericsParameterValidations
    {
        private const string _parameterNameName = "parameterName";
        private const string _conditionName = "condition";
        private const string _messageName = "message";
        private const string _messageParametersName = "messageParameters";

        /// <summary>
        /// Validate if <paramref name="parameterValue"/> is not <see langword="null"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <paramref name="parameterValue"/>. Must be a reference type.</typeparam>
        /// <param name="parameterValue">The parameter that must be validated.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="parameterValue"/> is <see langword="null"/> or <paramref name="parameterName"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="parameterName"/> is <see cref="string.Empty"/>.</exception>
        [AssertionMethod]
        [ContractAnnotation("parameterValue:null => void; parameterName:null => void")]
        public static void AssertIsNotNull<T>(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this T parameterValue,
            [NotNull, InvokerParameterName] string parameterName)
            where T : class
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotEmpty(parameterName, _parameterNameName);

            Validators.ValidateNotNull(parameterValue, parameterName);
        }


        /// <overloads>
        /// <para>
        /// Validate if the <paramref name="parameterValue"/> meets the criteria in the predicate <paramref name="condition"/>.
        /// </para>
        /// <para>
        /// The validation <see cref="AssertMeetsCondition{T}(T,System.Predicate{T},string)"/> can be used to validate if a parameter meets a condition. 
        /// The validation <see cref="AssertMeetsCondition{T}(T,System.Predicate{T},string,string,object[])"/> does the same, but allows for a custom message that is set as the message 
        /// for the exception if the condition in <paramref name="condition"/> is not met.
        /// </para>
        /// </overloads>
        /// <summary>
        /// Validate if the <paramref name="parameterValue"/> meets the criteria in the predicate <paramref name="condition"/>,
        /// providing a custom <paramref name="message"/> for the <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <typeparam name="T">The type of <paramref name="parameterValue"/>.</typeparam>
        /// <param name="parameterValue">The parameter that must be validated.</param>
        /// <param name="condition">The <see cref="Predicate{T}"/> that must be met. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="message">
        /// The message to give in the <see cref="ArgumentOutOfRangeException"/> if the condition in <paramref name="condition"/> is not met. 
        /// The message may be a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.
        /// Must not be <see langword="null"/> or <see cref="string.Empty"/>.
        /// </param>
        /// <param name="messageParameters">The message parameters, if <paramref name="message"/> is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.</param>
        /// <remarks>
        /// <para>
        /// The value of <paramref name="parameterValue"/> will be passed in to the <see cref="Predicate{T}"/> in <paramref name="condition"/>. 
        /// If the <see cref="Predicate{T}"/> in <paramref name="condition"/> returns <see langword="false"/>, an <see cref="ArgumentOutOfRangeException"/> will be thrown with the message defined by 
        /// <paramref name="message"/> and its <paramref name="messageParameters"/>.
        /// </para>
        /// <para>
        /// If the message is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>, 
        /// then it can be formatted in the same way as for <see cref="string.Format(string,object[])"/>. 
        /// </para>
        /// <example>
        /// <para>
        /// This example shows the use of this method on parameter 'param1' to validate that the value is between 1 and 4.
        /// </para>
        /// <code lang="C#">
        /// <![CDATA[
        /// public void foo(int param1)
        /// {
        ///     param1.AssertMeetsCondition(val => val >= 1 && val <= 4, "param1", "value must be between 1 and 4 but was {0}", param1);
        /// 
        ///     ...
        /// }
        /// ]]>
        /// </code>
        /// <para>
        /// Calling this method from the following code will result in a <see cref="ArgumentOutOfRangeException"/> being thrown.
        /// </para>
        /// <code lang="C#">
        ///     int bar = 5;
        ///     x.foo(bar);
        ///     // Execution will not get here, as an ArgumentOutOfRangeException is thrown in the previous line.
        /// </code>
        /// </example>
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="condition"/> is <see langword="null"/>, <paramref name="parameterName"/> is <see langword="null"/>, 
        /// or <paramref name="message"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="parameterValue"/> does not match the condition in <paramref name="condition"/>, <paramref name="parameterName"/> is <see cref="string.Empty"/>, 
        /// <paramref name="message"/> is <see cref="string.Empty"/>.
        /// </exception>
        [ContractAnnotation("condition:null => void; parameterName:null => void; message:null => void; messageParameters:null => void")]
        [StringFormatMethod("message")]
        public static void AssertMeetsCondition<T>(this T parameterValue,
            [NotNull] Predicate<T> condition,
            [NotNull, InvokerParameterName] string parameterName,
            [NotNull] string message,
            [NotNull] params object[] messageParameters)
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(condition, _conditionName);
            Validators.ValidateNotNull(message, _messageName);
            Validators.ValidateNotNull(messageParameters, _messageParametersName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);
            Validators.ValidateNotEmpty(message, _messageName);

            if (!condition(parameterValue))
            {
                string formattedMessage = string.Format(message, messageParameters);
                throw new ArgumentOutOfRangeException(parameterName, parameterValue, formattedMessage);
            }
        }

        /// <summary>
        /// Validate if the <paramref name="parameterValue"/> meets the criteria in the predicate <paramref name="condition"/>.
        /// </summary>
        /// <typeparam name="T">The type the <paramref name="parameterValue"/>.</typeparam>
        /// <param name="parameterValue">The parameter that must be validated.</param>
        /// <param name="condition">The <see cref="Predicate{T}"/> that must be met. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <remarks>
        /// <para>
        /// The value of <paramref name="parameterValue"/> will be passed in to the <see cref="Predicate{T}"/> in <paramref name="condition"/>. 
        /// If the <see cref="Predicate{T}"/> in <paramref name="condition"/> returns <see langword="false"/>, an <see cref="ArgumentOutOfRangeException"/> will be thrown.
        /// </para>
        /// <example>
        /// <para>
        /// This example shows the use of this method on parameter 'param1' to validate that the value is between 1 and 4.
        /// </para>
        /// <code lang="C#">
        /// <![CDATA[
        /// public void foo(int param1)
        /// {
        ///     param1.AssertMeetsCondition(val => val >= 1 && val <= 4, "param1");
        /// 
        ///     ...
        /// }
        /// ]]>
        /// </code>
        /// <para>
        /// Calling this method from the following code will result in a <see cref="ArgumentOutOfRangeException"/> being thrown.
        /// </para>
        /// <code lang="C#">
        ///     int bar = 5;
        ///     x.foo(bar);
        ///     // Execution will not get here, as an ArgumentOutOfRangeException is thrown in the previous line.
        /// </code>
        /// </example>
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="condition"/> is <see langword="null"/> or <paramref name="parameterName"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="parameterValue"/> does not match the condition in <paramref name="condition"/> or <paramref name="parameterName"/> is <see cref="string.Empty"/>.
        /// </exception>
        [ContractAnnotation("condition:null => void; parameterName:null => void")]
        public static void AssertMeetsCondition<T>(this T parameterValue,
            [NotNull] Predicate<T> condition,
            [NotNull, InvokerParameterName] string parameterName)
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(condition, _conditionName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);

            if (!condition(parameterValue))
            {
               throw new ArgumentOutOfRangeException(parameterName, parameterValue, "");
            }
        }

        /// <overloads>
        /// <para>
        /// Validate if the <paramref name="parameter"/> is not <see langword="null"/> and meets the criteria in the predicate <paramref name="condition"/>.
        /// </para>
        /// <para>
        /// The validation <see cref="AssertIsNotNullAndMeetsCondition{T}(T,System.Predicate{T},string)"/> can be used to validate if a parameter is not <see langword="null"/> and meets a condition. 
        /// The validation <see cref="AssertIsNotNullAndMeetsCondition{T}(T,System.Predicate{T},string,string,object[])"/> does the same, but allows for a custom message that is set as the message 
        /// for the exception if the condition in <paramref name="condition"/> is not met.
        /// </para>
        /// </overloads>
        /// <summary>
        /// Validate if the <paramref name="parameter"/> is not <see langword="null"/> and meets the criteria in the predicate <paramref name="condition"/>,
        /// providing a custom <paramref name="message"/> for the <see cref="ArgumentOutOfRangeException"/>.
        /// </summary>
        /// <typeparam name="T">The type of <paramref name="parameter"/>.</typeparam>
        /// <param name="parameter">The parameter that must be validated.</param>
        /// <param name="condition">The <see cref="Predicate{T}"/> that must be met. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="message">
        /// The message to give in the <see cref="ArgumentOutOfRangeException"/> if the condition in <paramref name="condition"/> is not met. 
        /// The message may be a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.
        /// Must not be <see langword="null"/> or <see cref="string.Empty"/>.
        /// </param>
        /// <param name="messageParameters">The message parameters, if <paramref name="message"/> is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.</param>
        /// <remarks>
        /// <para>
        /// The value of <paramref name="parameter"/> will be passed in to the <see cref="Predicate{T}"/> in <paramref name="condition"/>, if <paramref name="parameter"/> is not <see langword="null"/>. 
        /// If the <see cref="Predicate{T}"/> in <paramref name="condition"/> returns <see langword="false"/>, an <see cref="ArgumentOutOfRangeException"/> will be thrown with the message defined by 
        /// <paramref name="message"/> and its <paramref name="messageParameters"/>.
        /// </para>
        /// <para>
        /// If the message is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>, 
        /// then it can be formatted in the same way as for <see cref="string.Format(string,object[])"/>. 
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="parameter"/> is <see langword="null"/>, <paramref name="condition"/> is <see langword="null"/>, 
        /// <paramref name="parameterName"/> is <see langword="null"/>, or <paramref name="message"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="parameter"/> does not match the condition in <paramref name="condition"/>, <paramref name="parameterName"/> is <see cref="string.Empty"/>, 
        /// <paramref name="message"/> is <see cref="string.Empty"/>.
        /// </exception>
        [StringFormatMethod("message")]
        [AssertionMethod]
        [ContractAnnotation("parameter:null => void; condition:null => void; parameterName:null => void; message:null => void; messageParameters:null => void")]
        public static void AssertIsNotNullAndMeetsCondition<T>(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this T parameter,
            [NotNull] Predicate<T> condition,
            [NotNull, InvokerParameterName] string parameterName,
            [NotNull] string message,
            [NotNull] params object[] messageParameters) 
            where T : class
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(condition, _conditionName);
            Validators.ValidateNotNull(message, _messageName);
            Validators.ValidateNotNull(messageParameters, _messageParametersName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);
            Validators.ValidateNotEmpty(message, _messageName);

            Validators.ValidateNotNull(parameter, parameterName);

            parameter.AssertMeetsCondition(condition, parameterName, message, messageParameters);
        }


        /// <summary>
        /// Validate if the <paramref name="parameter"/> is not <see langword="null"/> and meets the criteria in the predicate <paramref name="condition"/>.
        /// </summary>
        /// <typeparam name="T">The type of <paramref name="parameter"/>.</typeparam>
        /// <param name="parameter">The parameter that must be validated.</param>
        /// <param name="condition">The <see cref="Predicate{T}"/> that must be met. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the validated parameter. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <remarks>
        /// <para>
        /// The value of <paramref name="parameter"/> will be passed in to the <see cref="Predicate{T}"/> in <paramref name="condition"/>, if <paramref name="parameter"/> is not <see langword="null"/>. 
        /// If the <see cref="Predicate{T}"/> in <paramref name="condition"/> returns <see langword="false"/>, an <see cref="ArgumentOutOfRangeException"/> will be thrown.
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="parameter"/> is <see langword="null"/>, <paramref name="condition"/> is <see langword="null"/>, 
        /// or <paramref name="parameterName"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="parameter"/> does not match the condition in <paramref name="condition"/> or <paramref name="parameterName"/> is <see cref="string.Empty"/>.
        /// </exception>
        [AssertionMethod]
        [ContractAnnotation("parameter:null => void; condition:null => void; parameterName:null => void")]
        public static void AssertIsNotNullAndMeetsCondition<T>(
            [NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] this T parameter,
            [NotNull] Predicate<T> condition,
            [NotNull, InvokerParameterName] string parameterName)
            where T : class
        {
            Validators.ValidateNotNull(parameterName, _parameterNameName);
            Validators.ValidateNotNull(condition, _conditionName);

            Validators.ValidateNotEmpty(parameterName, _parameterNameName);

            Validators.ValidateNotNull(parameter, parameterName);

            parameter.AssertMeetsCondition(condition, parameterName);
        }
    }
}