﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace Twia.Validation
{
    /// <summary>
    /// A <see cref="string"/> argument to a method or the value on a string property setter did not match a regular expression.
    /// </summary>
    /// <remarks>
    /// <para>
    /// The name of the parameter that caused the <see cref="ArgumentDoesNotMatchRegexException"/> to be thrown can be retrieved through the <see cref="ArgumentException.ParamName"/> property.
    /// The value of the parameter can be retrieved through the <see cref="Value"/> property and the used regular expression can be retrieved through the <see cref="Regex"/> property.
    /// </para>
    /// </remarks>
    [Serializable]
    public sealed class ArgumentDoesNotMatchRegexException : ArgumentException
    {
        private const string _paramNameName = "paramName";
        private const string _valueName = "value";
        private const string _regexName = "regex";
        private const string _formatName = "format";

        // Names for the values in serialization.
        private const string _paramValueDataName = "ParamValue";
        private const string _regexDataName = "Regex";


        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentDoesNotMatchRegexException"/> class with a given offending parameter name in <paramref name="paramName"/>, 
        /// its value in <paramref name="value"/> and the used regular expression in <paramref name="regex"/>.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused this exception. Must not be <see langword="null"/> or <see cref="String.Empty"/>.</param>
        /// <param name="value">The value that did not match the regular expression in <paramref name="regex"/>.</param>
        /// <param name="regex">The regular expression used to test the value in <paramref name="value"/>.</param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="paramName"/> is <see langword="null"/>, 
        ///     <paramref name="value"/> is <see langword="null"/>, or
        ///     <paramref name="regex"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentEmptyException"><paramref name="paramName"/> is <see cref="String.Empty"/> or <paramref name="regex"/> is <see cref="String.Empty"/>.</exception>
        public ArgumentDoesNotMatchRegexException([NotNull] string paramName, [NotNull] string value, [NotNull] string regex)
            : base(Resources.Messages.MessageArgumentRegexMismatch, paramName)
        {
            Validators.ValidateNotNull(paramName, _paramNameName);
            Validators.ValidateNotNull(value, _valueName);
            Validators.ValidateNotNull(regex, _regexName);

            Validators.ValidateNotEmpty(paramName, _paramNameName);
            Validators.ValidateNotEmpty(regex, _regexName);

            Value = value;
            Regex = regex;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentDoesNotMatchRegexException"/> class with a given offending parameter name in <paramref name="paramName"/>, 
        /// its value in <paramref name="value"/> and the used regular expression in <paramref name="regex"/>. Provide a custom message through <paramref name="format"/>.
        /// </summary>
        /// <param name="paramName">The name of the parameter that caused this exception. Must not be <see langword="null"/> or <see cref="String.Empty"/>.</param>
        /// <param name="value">The value that did not match the regular expression in <paramref name="regex"/>.</param>
        /// <param name="regex">The regular expression used to test the value in <paramref name="value"/>.</param>
        /// <param name="format">
        /// The custom message for the exception. The message may be a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.
        /// Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="args">The message parameters, if <paramref name="format"/> is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>.</param>
        /// <remarks>
        /// <para>
        /// If the message in <paramref name="format"/> is a <see href="http://msdn.microsoft.com/en-us/library/txafckwd(v=vs.100).aspx">composite format string</see>, 
        /// then it can be formatted in the same way as for <see cref="string.Format(string,object[])"/>. 
        /// </para>
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="paramName" /> is <see langword="null"/>, <paramref name="value" /> is <see langword="null"/>, <paramref name="regex" /> is <see langword="null"/>,
        /// <paramref name="format" /> is <see langword="null"/>, or <paramref name="args" /> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="ArgumentEmptyException">
        /// <paramref name="paramName" /> is <see cref="String.Empty"/>, <paramref name="regex" /> is <see cref="String.Empty"/>, or <paramref name="format" /> is <see cref="String.Empty"/>.
        /// </exception>
        /// <exception cref="FormatException"><paramref name="format" /> is invalid or the index of a format item is less than zero, or greater than or equal to the length of the <paramref name="args" /> array.</exception>
        public ArgumentDoesNotMatchRegexException([NotNull] string paramName, [NotNull] string value, [NotNull] string regex, [NotNull] string format, [NotNull] params object[] args)
            : base(string.Format(format, args), paramName)
        {
            Validators.ValidateNotNull(paramName, _paramNameName);
            Validators.ValidateNotNull(value, _valueName);
            Validators.ValidateNotNull(regex, _regexName);

            Validators.ValidateNotEmpty(paramName, _paramNameName);
            Validators.ValidateNotEmpty(regex, _regexName);
            Validators.ValidateNotEmpty(format, _formatName);

            Value = value;
            Regex = regex;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArgumentEmptyException"/> class with serialized data.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        /// <remarks>
        /// This constructor is called during deserialization to reconstitute the exception object transmitted over a stream. 
        /// For more information, see <see href="http://msdn.microsoft.com/en-us/library/vstudio/90c86ass(v=vs.100).aspx">XML and SOAP Serialization</see>.
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="info"/> is <see langword="null"/>.</exception>
        private ArgumentDoesNotMatchRegexException([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Value = info.GetString(_paramValueDataName);
            Regex = info.GetString(_regexDataName);
        }



        /// <summary>
        /// Read-only property to get the value that did not match the regular expression.
        /// </summary>
        /// <value>
        /// The value that did not match the regular expression in <see cref="Regex"/>.
        /// </value>
        public string Value { get; private set; }

        /// <summary>
        /// Read-only property to get the regular expression that was used to test the value.
        /// </summary>
        /// <value>
        /// The regular expression that was used to test the value in <see cref="Value"/>.
        /// </value>
        public string Regex { get; private set; }


        /// <summary>
        /// Sets the <see cref="SerializationInfo"/> object with the parameter name, parameter value, regular expression and additional exception information.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        /// <exception cref="ArgumentNullException"><paramref name="info"/> is <see langword="null"/>. </exception>
        /// <PermissionSet>
        /// <IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Read="*AllFiles*" PathDiscovery="*AllFiles*"/>
        /// </PermissionSet>
        [System.Security.SecurityCritical]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue(_paramValueDataName, Value, typeof(string));
            info.AddValue(_regexDataName, Regex, typeof(string));
        }
    }
}