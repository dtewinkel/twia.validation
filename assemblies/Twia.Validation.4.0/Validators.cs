﻿using System;
using System.Text.RegularExpressions;
using JetBrains.Annotations;

namespace Twia.Validation
{
    internal static class Validators
    {
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null"/>.</exception>
        // ReSharper disable once UnusedParameter.Global
        [AssertionMethod]
        [ContractAnnotation("value:null => void")]
        internal static void ValidateNotNull<T>([NotNull, AssertionCondition(AssertionConditionType.IS_NOT_NULL)] T value, [NotNull] string parameterName) 
            where T: class 
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }

        /// <exception cref="ArgumentEmptyException"><paramref name="value"/> is <see cref="string.Empty"/>.</exception>
        // ReSharper disable once UnusedParameter.Global
        internal static void ValidateNotEmpty(string value, string parameterName)
        {
            if (value == String.Empty)
            {
                throw new ArgumentEmptyException(parameterName);
            }
        }

        /// <exception cref="ArgumentDoesNotMatchRegexException"><paramref name="value"/> does not match the regular expression in <paramref name="regex"/>.</exception>
        /// <exception cref="ArgumentException">A regular expression parsing error occurred.</exception>
        public static void ValidateMatchesRegex(string value, string regex, string parameterName)
        {
            if (!Regex.IsMatch(value, regex))
            {
                throw new ArgumentDoesNotMatchRegexException(parameterName, value, regex);
            }
        }

        /// <exception cref="ArgumentDoesNotMatchRegexException"><paramref name="value"/> does not match the regular expression in <paramref name="regex"/>.</exception>
        /// <exception cref="ArgumentException">A regular expression parsing error occurred.</exception>
        /// <exception cref="FormatException"><paramref name="message" /> is invalid or the index of a format item is less than zero, or greater than or equal to the length of the <paramref name="messageParameters" /> array.</exception>
        public static void ValidateMatchesRegex(string value, string regex, string parameterName, string message, params object[] messageParameters)
        {
            if (!Regex.IsMatch(value, regex))
            {
                throw new ArgumentDoesNotMatchRegexException(parameterName, value, regex, message, messageParameters);
            }
        }
    }
}