﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Twia.Validation")]
[assembly: AssemblyDescription("Provides parameter validation extension methods.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TWIA")]
[assembly: AssemblyProduct("Twia.Validation")]
[assembly: AssemblyCopyright("Copyright © 2013 TWIA, All rights reserved")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("66647395-9593-47a4-a80a-9ad77641aa64")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.0.37")]
[assembly: AssemblyVersion("1.0.0.37")]
[assembly: AssemblyFileVersion("1.0.0.37")]
