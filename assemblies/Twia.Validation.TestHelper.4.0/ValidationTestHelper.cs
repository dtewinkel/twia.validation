﻿using System;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Twia.Validation.TestHelper
{
    /// <summary>
    /// Helper methods to validate the validation of method parameters, typically through the extension methods in <see cref="Twia.Validation"/>.
    /// </summary>
    /// <remarks>
    /// This class depends on NUnit to do its assertions.
    /// </remarks>
    public static class ExceptionValidation
    {
        /// <summary>
        /// Validate if <paramref name="action"/> throws an <see cref="ArgumentEmptyException"/> and validate the parameter name in the exception.
        /// </summary>
        /// <param name="action">The action to execute that must throw the exception. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the parameter that must be set in the exception. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <returns>The <see cref="ArgumentEmptyException"/> thrown by <paramref name="action"/>.</returns>
        [NotNull]
        public static ArgumentEmptyException ValidateArgumentEmptyException([NotNull, InstantHandle] Action action, [NotNull] string parameterName)
        {
            action.AssertIsNotNull("action");
            parameterName.AssertIsNotNullOrEmpty("parameterName");

            ArgumentEmptyException exception = Assert.Throws<ArgumentEmptyException>(() => action());
            Assert.That(exception.ParamName, Is.EqualTo(parameterName));
            return exception;
        }


        /// <summary>
        /// Validate if <paramref name="action"/> throws an <see cref="ArgumentOutOfRangeException"/> and validate the parameter name and actual value in the exception.
        /// </summary>
        /// <param name="action">The action to execute that must throw the exception. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the parameter that must be set in the exception. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <param name="actualValue">The actual value that must be set in the exception. Defaults to <see langword="null"/>.</param>
        /// <returns>The <see cref="ArgumentOutOfRangeException"/> thrown by <paramref name="action"/>.</returns>
        [NotNull]
        public static ArgumentOutOfRangeException ValidateArgumentOutOfRangeException([NotNull, InstantHandle] Action action, [NotNull] string parameterName, [CanBeNull] object actualValue = null)
        {
            action.AssertIsNotNull("action");
            parameterName.AssertIsNotNullOrEmpty("parameterName");

            ArgumentOutOfRangeException exception = Assert.Throws<ArgumentOutOfRangeException>(() => action());
            Assert.That(exception.ParamName, Is.EqualTo(parameterName));
            Assert.That(exception.ActualValue, Is.EqualTo(actualValue));
            return exception;
        }


        /// <summary>
        /// Validate if <paramref name="action"/> throws an <see cref="ArgumentOutOfRangeException"/> and validate the parameter name in the exception.
        /// </summary>
        /// <param name="action">The action to execute that must throw the exception. Must not be <see langword="null"/>.</param>
        /// <param name="parameterName">The name of the parameter that must be set in the exception. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <returns>The <see cref="ArgumentNullException"/> thrown by <paramref name="action"/>.</returns>
        /// <exception cref="AssertionException"><paramref name="action"/> did not throw an <see cref="ArgumentNullException"/>.</exception>
        [NotNull]
        public static ArgumentNullException ValidateArgumentNullException([NotNull, InstantHandle] Action action, [NotNull] string parameterName)
        {
            action.AssertIsNotNull("action");
            parameterName.AssertIsNotNullOrEmpty("parameterName");

            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => action());
            Assert.That(exception.ParamName, Is.EqualTo(parameterName));
            return exception;
        }
    }
}
