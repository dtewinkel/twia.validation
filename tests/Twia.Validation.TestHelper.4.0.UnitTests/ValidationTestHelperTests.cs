﻿using System;
using NUnit.Framework;

namespace Twia.Validation.TestHelper.UnitTests
{
    [TestFixture]
    public class ValidationTestHelperTests
    {
        private const string _variableName1 = "myFirstVariable";
        private const string _variableName2 = "aSecondVariable";

        private const string _giveValue = "Some text";
        private const string _actualValue = "Some text";
        private const string _actualValueMessage = "Wrong Text!";

        private readonly Action _nullExceptionAction = () =>
        {
            throw new ArgumentNullException(_variableName1);
        };

        private readonly Action _emptyExceptionAction = () =>
        {
            throw new ArgumentEmptyException(_variableName1);
        };

        private readonly Action _outOfRangeExceptionAction = () =>
        {
            throw new ArgumentOutOfRangeException(_variableName1);
        };

        private readonly Action _outOfRangeExceptionWithValueAction = () =>
        {
            throw new ArgumentOutOfRangeException(_variableName1, _actualValue, _actualValueMessage);
        };

        private readonly Action<string> _dualExceptionWithValueAction = val =>
        {
            if (val != null)
            {
                throw new ArgumentOutOfRangeException(_variableName1, val, _actualValueMessage);
            }
            throw new ArgumentNullException(_variableName1);
        };

        private readonly Action<string> _dualExceptionWithoutValueAction = val =>
        {
            if (val != null)
            {
                throw new ArgumentOutOfRangeException(_variableName1);
            }
            throw new ArgumentNullException(_variableName1);
        };

        private readonly Action _noAction = () =>
        {
            
        };

        #region ValidateArgumentEmptyException.

        [Test]
        public void ValidateArgumentEmptyExceptionWrongParameters()
        {
            // Prepare. 

            // Act.
            // ReSharper disable AssignNullToNotNullAttribute
            ArgumentNullException exception1 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentEmptyException(null, _variableName1));
            ArgumentNullException exception2 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentEmptyException(_emptyExceptionAction, null));
            // ReSharper restore AssignNullToNotNullAttribute
            ArgumentEmptyException exception3 = Assert.Throws<ArgumentEmptyException>(() => ExceptionValidation.ValidateArgumentEmptyException(_emptyExceptionAction, ""));

            // Validate.
            Assert.That(exception1.ParamName, Is.EqualTo("action"));
            Assert.That(exception2.ParamName, Is.EqualTo("parameterName"));
            Assert.That(exception3.ParamName, Is.EqualTo("parameterName"));
        }

        [Test]
        public void ValidateArgumentEmptyException()
        {
            // Prepare. 

            // Act.
            ArgumentEmptyException validateArgumentEmptyException = ExceptionValidation.ValidateArgumentEmptyException(_emptyExceptionAction, _variableName1);

            // Validate.
            Assert.That(validateArgumentEmptyException, Is.Not.Null);
            Assert.That(validateArgumentEmptyException.ParamName, Is.EqualTo(_variableName1));
        }


        [Test]
        public void ValidateArgumentEmptyExceptionExceptionNotThrown()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentEmptyException(_noAction, _variableName1));

            // Validate.
            Assert.That(excption.Message, Contains.Substring("Expected: <Twia.Validation.ArgumentEmptyException>"));
            Assert.That(excption.Message, Contains.Substring("But was:  null"));
        }


        [Test]
        public void ValidateArgumentEmptyExceptionWrongParameterName()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentEmptyException(_emptyExceptionAction, _variableName2));

            // Validate.
            Assert.That(excption.Message, Contains.Substring(string.Format("Expected: \"{0}\"", _variableName2)));
            Assert.That(excption.Message, Contains.Substring(string.Format("But was:  \"{0}\"", _variableName1)));
        }

        #endregion


        #region ValidateArgumentNullException.

        [Test]
        public void ValidateArgumentNullExceptionWrongParameters()
        {
            // Prepare. 

            // Act.
// ReSharper disable AssignNullToNotNullAttribute
            ArgumentNullException exception1 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentNullException(null, _variableName1));
            ArgumentNullException exception2 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentNullException(_nullExceptionAction, null));
// ReSharper restore AssignNullToNotNullAttribute
            ArgumentEmptyException exception3 = Assert.Throws<ArgumentEmptyException>(() => ExceptionValidation.ValidateArgumentNullException(_nullExceptionAction, ""));

            // Validate.
            Assert.That(exception1.ParamName, Is.EqualTo("action"));
            Assert.That(exception2.ParamName, Is.EqualTo("parameterName"));
            Assert.That(exception3.ParamName, Is.EqualTo("parameterName"));
        }

        [Test]
        public void ValidateArgumentNullException()
        {
            // Prepare. 

            // Act.
            ArgumentNullException validateArgumentNullException = ExceptionValidation.ValidateArgumentNullException(_nullExceptionAction, _variableName1);

            // Validate.
            Assert.That(validateArgumentNullException, Is.Not.Null);
            Assert.That(validateArgumentNullException.ParamName, Is.EqualTo(_variableName1));
        }


        [Test]
        public void ValidateArgumentNullExceptionExceptionNotThrown()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentNullException(_noAction, _variableName1));

            // Validate.
            Assert.That(excption.Message, Contains.Substring("Expected: <System.ArgumentNullException>"));
            Assert.That(excption.Message, Contains.Substring("But was:  null"));
        }


        [Test]
        public void ValidateArgumentNullExceptionWrongParameterName()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentNullException(_nullExceptionAction, _variableName2));

            // Validate.
            Assert.That(excption.Message, Contains.Substring(string.Format("Expected: \"{0}\"", _variableName2)));
            Assert.That(excption.Message, Contains.Substring(string.Format("But was:  \"{0}\"", _variableName1)));
        }

        #endregion

        #region ValidateArgumentOutOfRangeException.

        [Test]
        public void ValidateArgumentOutOfRangeExceptionWrongParameters()
        {
            // Prepare. 

            // Act.
            // ReSharper disable AssignNullToNotNullAttribute
            ArgumentNullException exception1 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(null, _variableName1));
            ArgumentNullException exception2 = Assert.Throws<ArgumentNullException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionAction, null));
            // ReSharper restore AssignNullToNotNullAttribute
            ArgumentEmptyException exception3 = Assert.Throws<ArgumentEmptyException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionAction, ""));

            // Validate.
            Assert.That(exception1.ParamName, Is.EqualTo("action"));
            Assert.That(exception2.ParamName, Is.EqualTo("parameterName"));
            Assert.That(exception3.ParamName, Is.EqualTo("parameterName"));
        }

        [Test]
        public void ValidateArgumentOutOfRangeException()
        {
            // Prepare. 

            // Act.
            ArgumentOutOfRangeException validateArgumentOutOfRangeException = ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionAction, _variableName1);

            // Validate.
            Assert.That(validateArgumentOutOfRangeException, Is.Not.Null);
            Assert.That(validateArgumentOutOfRangeException.ParamName, Is.EqualTo(_variableName1));
            Assert.That(validateArgumentOutOfRangeException.ActualValue, Is.Null);
        }

        [Test]
        public void ValidateArgumentOutOfRangeExceptionWithValue()
        {
            // Prepare. 

            // Act.
            ArgumentOutOfRangeException validateArgumentOutOfRangeException = ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionWithValueAction, _variableName1, _actualValue);

            // Validate.
            Assert.That(validateArgumentOutOfRangeException, Is.Not.Null);
            Assert.That(validateArgumentOutOfRangeException.ParamName, Is.EqualTo(_variableName1));
            Assert.That(validateArgumentOutOfRangeException.ActualValue, Is.EqualTo(_actualValue));
            Assert.That(validateArgumentOutOfRangeException.Message, Contains.Substring(_actualValueMessage));
        }


        [Test]
        public void ValidateArgumentOutOfRangeExceptionExceptionNotThrown()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(_noAction, _variableName1));

            // Validate.
            Assert.That(excption.Message, Contains.Substring("Expected: <System.ArgumentOutOfRangeException>"));
            Assert.That(excption.Message, Contains.Substring("But was:  null"));
        }


        [Test]
        public void ValidateArgumentOutOfRangeExceptionWrongParameterName()
        {
            // Prepare. 

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionAction, _variableName2));

            // Validate.
            Assert.That(excption.Message, Contains.Substring(string.Format("Expected: \"{0}\"", _variableName2)));
            Assert.That(excption.Message, Contains.Substring(string.Format("But was:  \"{0}\"", _variableName1)));
        }


        [Test]
        public void ValidateArgumentOutOfRangeExceptionWrongValue()
        {
            // Prepare. 
            const string wrongValue = "Other text";

            // Act.
            AssertionException excption = Assert.Throws<AssertionException>(() => ExceptionValidation.ValidateArgumentOutOfRangeException(_outOfRangeExceptionWithValueAction, _variableName1, wrongValue));

            // Validate.
            Assert.That(excption.Message, Contains.Substring(string.Format("Expected: \"{0}\"", wrongValue)));
            Assert.That(excption.Message, Contains.Substring(string.Format("But was:  \"{0}\"", _actualValue)));
        }

        #endregion
    }
}