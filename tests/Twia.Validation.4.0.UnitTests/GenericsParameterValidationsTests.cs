using System;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Twia.Validation.UnitTests
{
    [TestFixture]
    public class GenericsParameterValidationsTests
    {
        #region Fields.

        private readonly object _validObject = new object();

        #endregion

        #region Test set-up and tear down.

        #endregion

        #region Tests.

        #region AssertIsNotNull

        [Test]
        public void ValueIsOkTest()
        {
            TestObjectIsNotNull(_validObject);
        }


        [Test]
        public void ValueIsNullTest()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => TestObjectIsNotNull(null));
            Assert.That(exception.ParamName, Is.EqualTo("obj"));
        }


        [Test]
        public void ParameterNameIsNullTest()
        {
            // ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => _validObject.AssertIsNotNull(null));
            // ReSharper restore AssignNullToNotNullAttribute
        }


        [Test]
        public void ParameterNameIsEmptyTest()
        {
            Assert.Throws<ArgumentEmptyException>(() => _validObject.AssertIsNotNull(""));
        }

        #endregion

        #region AssertMeetsCondition

        [Test]
        public void AssertConditionMetTestOnAssertMeetsCondition()
        {
            AssertMeetsCondition(1, val => val > 0, "hi");
            AssertMeetsCondition(1, val => val > 0);
        }


        [Test]
        public void AssertConditionNotMetTestOnAssertMeetsCondition()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => AssertMeetsCondition(1, val => val < 0, "hi."));
            Assert.Throws<ArgumentOutOfRangeException>(() => AssertMeetsCondition(1, val => val < 0, "hi {0}.", "there"));
            ArgumentOutOfRangeException exception = Assert.Throws<ArgumentOutOfRangeException>(() => AssertMeetsCondition(1, val => val < 0));
            Assert.That(exception.ParamName, Is.EqualTo("value"));
        }


        [Test]
        public void ParameterNameIsNullOnAssertMeetsConditionTest()
        {
            // ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => 0.AssertMeetsCondition(val => val == 0, null));
            Assert.Throws<ArgumentNullException>(() => 0.AssertMeetsCondition(val => val == 0, null, "Message"));
            // ReSharper restore AssignNullToNotNullAttribute
        }


        [Test]
        public void ParameterNameIsEmptyOnAssertMeetsConditionTest()
        {
            Assert.Throws<ArgumentEmptyException>(() => 0.AssertMeetsCondition(val => val == 0, ""));
            Assert.Throws<ArgumentEmptyException>(() => 0.AssertMeetsCondition(val => val == 0, "", "Message"));
        }


        [Test]
        public void MessageIsNullOnAssertMeetsConditionTest()
        {
            Assert.Throws<ArgumentNullException>(() => AssertMeetsCondition(0, val => true, null));
        }


        [Test]
        public void MessageIsEmptyOnAssertMeetsConditionTest()
        {
            Assert.Throws<ArgumentEmptyException>(() => AssertMeetsCondition(0, val => true, ""));
        }


        [Test]
        public void ConditionIsNullOnAssertMeetsConditionTest()
        {
            Assert.Throws<ArgumentNullException>(() => AssertMeetsCondition(0, null, "Message"));
            Assert.Throws<ArgumentNullException>(() => AssertMeetsCondition(0, null));
        }

        #endregion

        #region AssertIsNotNullAndMeetsCondition

        [Test]
        public void AssertConditionMetTestOnAssertIsNotNullAndMeetsCondition()
        {
            AssertIsNotNullAndMeetsCondition("test", val => val.Length > 0, "hi");
            AssertIsNotNullAndMeetsCondition("test", val => val.Length > 0);
        }


        [Test]
        public void AssertConditionNotMetTestOnAssertIsNotNullAndMeetsCondition()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => AssertIsNotNullAndMeetsCondition("test", val => val.Length < 0, "hi"));
            Assert.Throws<ArgumentOutOfRangeException>(() => AssertIsNotNullAndMeetsCondition("test", val => val.Length < 0));
        }


        [Test]
        public void ValueNullOnAssertIsNotNullAndMeetsCondition()
        {
            string test = null;
            // ReSharper disable ExpressionIsAlwaysNull
            Assert.Throws<ArgumentNullException>(() => AssertIsNotNullAndMeetsCondition(test, val => val.Length != 0, "hi"));
            Assert.Throws<ArgumentNullException>(() => AssertIsNotNullAndMeetsCondition(test, val => val.Length != 0));
            // ReSharper restore ExpressionIsAlwaysNull
        }


        [Test]
        public void ParameterNameIsNullOnAssertIsNotNullAndMeetsConditionTest()
        {
            const string test = "test";
            // ReSharper disable AssignNullToNotNullAttribute
            Assert.Throws<ArgumentNullException>(() => test.AssertIsNotNullAndMeetsCondition(val => true, null));
            Assert.Throws<ArgumentNullException>(() => test.AssertIsNotNullAndMeetsCondition(val => true, null, "Message"));
            // ReSharper restore AssignNullToNotNullAttribute
        }


        [Test]
        public void ParameterNameIsEmptyOnAssertIsNotNullAndMeetsConditionTest()
        {
            const string test = "test";
            Assert.Throws<ArgumentEmptyException>(() => test.AssertIsNotNullAndMeetsCondition(val => true, ""));
            Assert.Throws<ArgumentEmptyException>(() => test.AssertIsNotNullAndMeetsCondition(val => true, "", "Message"));
        }


        [Test]
        public void MessageIsNullOnAssertIsNotNullAndMeetsConditionTest()
        {
            Assert.Throws<ArgumentNullException>(() => AssertIsNotNullAndMeetsCondition("test", val => true, null));
        }


        [Test]
        public void MessageIsEmptyOnAssertIsNotNullAndMeetsConditionTest()
        {
            Assert.Throws<ArgumentEmptyException>(() => AssertIsNotNullAndMeetsCondition("test", val => true, ""));
        }


        [Test]
        public void ConditionIsNullOnAssertIsNotNullAndMeetsConditionTest()
        {
            Assert.Throws<ArgumentNullException>(() => AssertIsNotNullAndMeetsCondition("test", null, "Message"));
            Assert.Throws<ArgumentNullException>(() => AssertIsNotNullAndMeetsCondition("test", null));
        }

        #endregion

        #endregion

        #region Helper methods.


        // ReSharper disable UnusedParameter.Local

        private static void AssertIsNotNullAndMeetsCondition<T>(T value, Predicate<T> condition) where T : class
        {
            value.AssertIsNotNullAndMeetsCondition(condition, "value");
        }


        private static void AssertIsNotNullAndMeetsCondition<T>(T value, Predicate<T> condition, string message) where T : class
        {
            value.AssertIsNotNullAndMeetsCondition(condition, "value", message);
        }


        // ReSharper enable UnusedParameter.Local

        [StringFormatMethod("message")]
        private static void AssertMeetsCondition<T>(T value, Predicate<T> condition, string message, params object[] parameters)
        {
            value.AssertMeetsCondition(condition, "value", message, parameters);
        }


        private static void TestObjectIsNotNull(object obj)
        {
            obj.AssertIsNotNull("obj");
        }


        private static void AssertMeetsCondition<T>(T value, Predicate<T> condition)
        {
            value.AssertMeetsCondition(condition, "value");
        }

        #endregion
    }
}