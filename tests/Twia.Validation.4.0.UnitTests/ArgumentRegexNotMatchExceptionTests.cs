﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;

namespace Twia.Validation.UnitTests
{
    [TestFixture]
    public class ArgumentRegexNotMatchExceptionTests
    {
        const string _paramName = "param1";
        const string _regEx = "abc";
        const string _value = "1a2b3c4";
        const string _format = "{0}";
        const string _arg = "Some Message";
        const string _message = _arg;

        [Test]
        public void PropertiesTest()
        {
            ArgumentDoesNotMatchRegexException exception = new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx);
            Assert.That(exception.ParamName, Is.EqualTo(_paramName));
            Assert.That(exception.Value, Is.EqualTo(_value));
            Assert.That(exception.Regex, Is.EqualTo(_regEx));
            Assert.That(exception.Message, Is.StringContaining(_paramName));
        }


        [Test]
        public void PropertiesWithMessageTest()
        {
            ArgumentDoesNotMatchRegexException exception = new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx, _format, _arg);
            Assert.That(exception.ParamName, Is.EqualTo(_paramName));
            Assert.That(exception.Value, Is.EqualTo(_value));
            Assert.That(exception.Regex, Is.EqualTo(_regEx));
            Assert.That(exception.Message, Is.StringStarting(_message));
            Assert.That(exception.Message, Is.StringContaining(_paramName));
        }


        [Test]
        public void BinarySerializableTest()
        {
            ArgumentDoesNotMatchRegexException expected = new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx);
            BinaryFormatter serializer = new BinaryFormatter();
            ArgumentDoesNotMatchRegexException actual;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, expected);
                memoryStream.Seek(0L, SeekOrigin.Begin);
                actual = serializer.Deserialize(memoryStream) as ArgumentDoesNotMatchRegexException;
            }
            Assert.IsNotNull(actual);
            Assert.That(actual.ParamName, Is.EqualTo(expected.ParamName));
            Assert.That(actual.Value, Is.EqualTo(expected.Value));
            Assert.That(actual.Regex, Is.EqualTo(expected.Regex));
            Assert.That(actual.Message, Is.EqualTo(expected.Message));
        }



        [Test]
        public void BinarySerializableWithMessageTest()
        {
            ArgumentDoesNotMatchRegexException expected = new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx, _format, _arg);
            BinaryFormatter serializer = new BinaryFormatter();
            ArgumentDoesNotMatchRegexException actual;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, expected);
                memoryStream.Seek(0L, SeekOrigin.Begin);
                actual = serializer.Deserialize(memoryStream) as ArgumentDoesNotMatchRegexException;
            }
            Assert.IsNotNull(actual);
            Assert.That(actual.ParamName, Is.EqualTo(expected.ParamName));
            Assert.That(actual.Value, Is.EqualTo(expected.Value));
            Assert.That(actual.Regex, Is.EqualTo(expected.Regex));
            Assert.That(actual.Message, Is.EqualTo(expected.Message));
        }


        [Test]
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void ParameterNameNullTest()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(null, _value, _regEx));
            Assert.That(exception.ParamName, Is.EqualTo("paramName"));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(_paramName, null, _regEx));
            Assert.That(exception.ParamName, Is.EqualTo("value"));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, null));
            Assert.That(exception.ParamName, Is.EqualTo("regex"));

            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(null, _value, _regEx, _format, _arg));
            Assert.That(exception.ParamName, Is.EqualTo("paramName"));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(_paramName, null, _regEx, _format, _arg));
            Assert.That(exception.ParamName, Is.EqualTo("value"));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx, null, _arg));
            Assert.That(exception.ParamName, Is.EqualTo("format"));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx, _format, null));
            Assert.That(exception.ParamName, Is.EqualTo("args"));
        }


        [Test]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void ParameterNameEmptyTest()
        {
            ArgumentEmptyException exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentDoesNotMatchRegexException("", _value, _regEx));
            Assert.That(exception.ParamName, Is.EqualTo("paramName"));
            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, ""));
            Assert.That(exception.ParamName, Is.EqualTo("regex"));

            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentDoesNotMatchRegexException("", _value, _regEx, _format, _arg));
            Assert.That(exception.ParamName, Is.EqualTo("paramName"));
            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, "", _format, _arg));
            Assert.That(exception.ParamName, Is.EqualTo("regex"));
            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentDoesNotMatchRegexException(_paramName, _value, _regEx, "", _arg));
            Assert.That(exception.ParamName, Is.EqualTo("format"));
        }
         
    }
}