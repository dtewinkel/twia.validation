using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using NUnit.Framework;
using Twia.Validation.TestHelper;

namespace Twia.Validation.UnitTests
{
    [TestFixture]
    public class StringParameterValidationsTests
    {
        private const string _testValue2 = "12";
        private const string _valueParamName = "value";
        private const string _paramParamName = "parameterName";
        private const string _messageParamName = "message";
        private const string _messageParametesParamName = "messageParameters";
        private const string _message = "{0}";
        private const string _messageParams = "My Message";

        #region AssertIsNotNullOrEmpty

        [Test]
        public void ValueIsOkOnAssertIsNotNullOrEmptyTest()
        {
            TestIsNotNullOrEmpty(_testValue2, _valueParamName);
            // No exception here. So we are fine.
        }


        [Test]
        public void ValueIsNullOnAssertIsNotNullOrEmptyTest()
        {
            ExceptionValidation.ValidateArgumentNullException(() => TestIsNotNullOrEmpty(null, _valueParamName), _valueParamName);
        }


        [Test]
        public void ValueIsEmptyOnAssertIsNotNullOrEmptyTest()
        {
            ExceptionValidation.ValidateArgumentEmptyException(() => TestIsNotNullOrEmpty("", _valueParamName), _valueParamName);
        }


        [Test]
        public void ParamNameIsNullOnAssertIsNotNullOrEmptyTest()
        {
            ExceptionValidation.ValidateArgumentNullException(() => TestIsNotNullOrEmpty(_testValue2, null), _paramParamName);
        }


        [Test]
        public void ParamNameIsEmptyOnAssertIsNotNullOrEmptyTest()
        {
            ExceptionValidation.ValidateArgumentEmptyException(() => TestIsNotNullOrEmpty(_testValue2, string.Empty), _paramParamName);
        }


        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        // ReSharper disable once UnusedParameter.Local
        private static void TestIsNotNullOrEmpty([CanBeNull] string value, [CanBeNull] string paramName)
        {
            value.AssertIsNotNullOrEmpty(paramName);
        }


        #endregion

        #region AssertIsNotNullAndMatchesRegex

        private const string _regex = @"^\d+$";
        private const string _correctValue = "456";
        private const string _wrongValue = "a1b2c3";
        private const string _regexParamName = "regex";

        [Test]
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void ParameterIsNullOnAssertIsNotNullAndMatchesRegexTest()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(null, _regex, _valueParamName));
            Assert.That(exception.ParamName, Is.EqualTo(_valueParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, null, _valueParamName));
            Assert.That(exception.ParamName, Is.EqualTo(_regexParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, null));
            Assert.That(exception.ParamName, Is.EqualTo(_paramParamName));

            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(null, _regex, _valueParamName, _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_valueParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, null, _valueParamName, _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_regexParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, null, _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_paramParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, _valueParamName, null, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_messageParamName));
            exception = Assert.Throws<ArgumentNullException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, _valueParamName, _message, null));
            Assert.That(exception.ParamName, Is.EqualTo(_messageParametesParamName));
        }


        [Test]
        public void ParameterIsEmptyOnAssertIsNotNullAndMatchesRegexTest()
        {
            Assert.DoesNotThrow(() => TestIsNotNullAndMatchesRegex("", ".*", _valueParamName));
            ArgumentEmptyException exception = Assert.Throws<ArgumentEmptyException>(() => TestIsNotNullAndMatchesRegex(_correctValue, "", _valueParamName));
            Assert.That(exception.ParamName, Is.EqualTo(_regexParamName));
            exception = Assert.Throws<ArgumentEmptyException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, ""));
            Assert.That(exception.ParamName, Is.EqualTo(_paramParamName));

            Assert.DoesNotThrow(() => TestIsNotNullAndMatchesRegex("", ".*", _valueParamName, _message, _messageParams));
            exception = Assert.Throws<ArgumentEmptyException>(() => TestIsNotNullAndMatchesRegex(_correctValue, "", _valueParamName, _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_regexParamName));
            exception = Assert.Throws<ArgumentEmptyException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, "", _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_paramParamName));
            exception = Assert.Throws<ArgumentEmptyException>(() => TestIsNotNullAndMatchesRegex(_correctValue, _regex, _valueParamName, "", _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_messageParamName));
        }


        [Test]
        public void ValueMatchesOnAssertIsNotNullAndMatchesRegexTest()
        {
            TestIsNotNullAndMatchesRegex(_correctValue, _regex, _valueParamName);
        }


        [Test]
        public void ValueMatchesOnAssertIsNotNullAndMatchesRegexWithMessageTest()
        {
            TestIsNotNullAndMatchesRegex(_correctValue, _regex, _valueParamName, _message, _messageParams);
        }


        [Test]
        public void ValueMatchesNotOnAssertIsNotNullAndMatchesRegexTest()
        {
            ArgumentDoesNotMatchRegexException exception = Assert.Throws<ArgumentDoesNotMatchRegexException>(() => TestIsNotNullAndMatchesRegex(_wrongValue, _regex, _valueParamName));
            Assert.That(exception.ParamName, Is.EqualTo(_valueParamName));
            Assert.That(exception.Value, Is.EqualTo(_wrongValue));
            Assert.That(exception.Regex, Is.EqualTo(_regex));
        }



        [Test]
        public void ValueMatchesNotOnAssertIsNotNullAndMatchesRegexWithMessageTest()
        {
            ArgumentDoesNotMatchRegexException exception = Assert.Throws<ArgumentDoesNotMatchRegexException>(() => TestIsNotNullAndMatchesRegex(_wrongValue, _regex, _valueParamName, _message, _messageParams));
            Assert.That(exception.ParamName, Is.EqualTo(_valueParamName));
            Assert.That(exception.Value, Is.EqualTo(_wrongValue));
            Assert.That(exception.Regex, Is.EqualTo(_regex));
        }


        // ReSharper disable UnusedParameter.Local
        private void TestIsNotNullAndMatchesRegex([CanBeNull] string value, [NotNull] string regex, [NotNull] string paramName)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            value.AssertIsNotNullAndMatchesRegex(regex, paramName);
        }


        // ReSharper disable UnusedParameter.Local
        private void TestIsNotNullAndMatchesRegex([CanBeNull] string value, [NotNull] string regex, [NotNull] string paramName, [NotNull] string message, [NotNull] params object[] parameters)
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            value.AssertIsNotNullAndMatchesRegex(regex, paramName, message, parameters);
        }


        // ReSharper restore UnusedParameter.Local

        #endregion
    }
}