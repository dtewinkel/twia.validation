﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NUnit.Framework;

namespace Twia.Validation.UnitTests
{
    [TestFixture]
    public class ArgumentEmptyExceptionTests
    {
        const string _paramName = "param1";
        const string _message = "There was a big error";
        const string _paramNameName = "paramName";
        const string _messageName = "message";
        readonly string _empty = string.Empty;

        [Test]
        public void ConstructorWithParamNameTest()
        {
            ArgumentEmptyException exception = new ArgumentEmptyException(_paramName);
            Assert.That(exception.ParamName, Is.EqualTo(_paramName));
            Assert.That(exception.Message, Is.StringContaining(_paramName));
        }

        [Test]
        public void ConstructorWithParamNameAndMessageTest()
        {
            ArgumentEmptyException exception = new ArgumentEmptyException(_paramName, _message);
            Assert.That(exception.ParamName, Is.EqualTo(_paramName));
            Assert.That(exception.Message, Is.StringStarting(_message));
            Assert.That(exception.Message, Is.StringContaining(_paramName));
        }


        [Test]
        public void BinarySerializableTest()
        {
            ArgumentEmptyException expected = new ArgumentEmptyException(_paramName);
            BinaryFormatter serializer = new BinaryFormatter();
            ArgumentEmptyException actual;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.Serialize(memoryStream, expected);
                memoryStream.Seek(0L, SeekOrigin.Begin);
                actual = serializer.Deserialize(memoryStream) as ArgumentEmptyException;
            }
            Assert.IsNotNull(actual);
            Assert.That(actual.ParamName, Is.EqualTo(expected.ParamName));
            Assert.That(actual.Message, Is.EqualTo(expected.Message));
        }


        [Test]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
        public void ParameterNameNullTest()
        {
            ArgumentNullException exception = Assert.Throws<ArgumentNullException>(() => new ArgumentEmptyException(null));
            Assert.That(exception.ParamName, Is.EqualTo(_paramNameName));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentEmptyException(null, _message));
            Assert.That(exception.ParamName, Is.EqualTo(_paramNameName));
            exception = Assert.Throws<ArgumentNullException>(() => new ArgumentEmptyException(_paramName, null));
            Assert.That(exception.ParamName, Is.EqualTo(_messageName));
        }


        [Test]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void ParameterNameEmptyTest()
        {
            ArgumentEmptyException exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentEmptyException(_empty));
            Assert.That(exception.ParamName, Is.EqualTo(_paramNameName));
            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentEmptyException(_empty, _message));
            Assert.That(exception.ParamName, Is.EqualTo(_paramNameName));
            exception = Assert.Throws<ArgumentEmptyException>(() => new ArgumentEmptyException(_paramName, _empty));
            Assert.That(exception.ParamName, Is.EqualTo(_messageName));
        }
    }
}