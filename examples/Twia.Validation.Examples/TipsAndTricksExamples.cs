﻿using System;
using JetBrains.Annotations;

namespace Twia.Validation.Examples
{
    /// <summary>
    /// Examples for the Tips and Tricks section.
    /// </summary>
    public class TipsAndTricksExamples
    {
        #region Document exceptions.

        /// <summary>
        /// Example to show the documentation of the parameter and exceptions.
        /// </summary>
        /// <param name="stringParameter">A string. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stringParameter"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="stringParameter"/> is <see cref="string.Empty"/></exception>
        public void DocumentExceptionsExample(string stringParameter)
        {
            // Validate the parameter.
            stringParameter.AssertIsNotNullOrEmpty("stringParameter");

            // Now we know the string is not null or empty, we can do something 
            // useful with it.
        }

        #endregion

        #region ReSharper Attributes Example.
        
        public void ReSharperAttributesExample([NotNull] string stringParameter)
        {
            // Validate the parameter.
            stringParameter.AssertIsNotNullOrEmpty("stringParameter");

            // Now we know the string is not null or empty, we can do something 
            // useful with it.
        }

        #endregion

        #region Document exceptions and ReSharper Attributes Example.

        /// <summary>
        /// Example to show the documentation of the parameter and exceptions.
        /// </summary>
        /// <param name="stringParameter">A string. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stringParameter"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="stringParameter"/> is <see cref="string.Empty"/></exception>
        public void CombinedExample([NotNull] string stringParameter)
        {
            // Validate the parameter.
            stringParameter.AssertIsNotNullOrEmpty("stringParameter");

            // Now we know the string is not null or empty, we can do something 
            // useful with it.
        }

        #endregion
    }
}