﻿using System;
using System.IO;
using JetBrains.Annotations;

namespace Twia.Validation.Examples
{
    /// <summary>
    /// Examples using the generics validations.
    /// </summary>
    public class GenericsExamples
    {
        #region Constructor example

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericsExamples"/> class, providing the output file for later use.
        /// </summary>
        /// <param name="outputFile">A input parameter. Must not be <see langword="null"/>.</param>
        /// <remarks>
        /// Detailed documentation of this constructor.
        /// </remarks>
        /// <exception cref="ArgumentNullException"><paramref name="outputFile"/> is <see langword="null"/></exception>
        public GenericsExamples([NotNull] StreamWriter outputFile)
        {
            // Validate the parameter.
            outputFile.AssertIsNotNull("outputFile");

            // Store it for later use.
            _outputFile = outputFile;
        }

        #endregion

        #region Property Example

        private StreamWriter _outputFile;

        /// <summary>
        /// Example property that uses the <see cref="GenericsParameterValidations.AssertIsNotNull{T}"/> validation.
        /// </summary>
        /// <value>
        /// A output file. Shall not be <see langword="null"/>.
        /// </value>
        /// <exception cref="ArgumentNullException"><paramref name="value"/> is <see langword="null"/></exception>
        [NotNull]
        public StreamWriter OutputFile
        {
            get
            {
                return _outputFile;
            }
            set
            {
                // Validate the input value.
                value.AssertIsNotNull("value");

                // Set it to the backing variable.
                _outputFile = value;
            }
        }

        #endregion

        #region Method Example

        /// <summary>
        /// Example method with parameter validation.
        /// </summary>
        /// <param name="outputFile">A output file. Shall not be <see langword="null"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="outputFile"/> is <see langword="null"/></exception>
        public void SetOutputFile([NotNull] StreamWriter outputFile)
        {
            // Validate the input value.
            outputFile.AssertIsNotNull("outputFile");

            // Set it to the backing variable.
            _outputFile = outputFile;
        }

        #endregion
    }
}