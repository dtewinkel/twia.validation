﻿using System;
using JetBrains.Annotations;

namespace Twia.Validation.Examples
{
    /// <summary>
    /// Provides examples for the string validations.
    /// </summary>
    public class StringExamples
    {
        #region String not null or empty example.

        /// <summary>
        /// Example to show the the AssertIsNotNullOrEmpty validation on a string.
        /// </summary>
        /// <param name="stringParameter">A string. Must not be <see langword="null"/> or <see cref="string.Empty"/>.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stringParameter"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="stringParameter"/> is <see cref="string.Empty"/></exception>
        public void StringNotNullOrEmptyExample([NotNull] string stringParameter)
        {
            // Validate the parameter.
            stringParameter.AssertIsNotNullOrEmpty("stringParameter");

            // Now we know the string is not null or empty, we can do something 
            // useful with it.
        }

        #endregion

        #region String Not null and matches regular expression.

        /// <summary>
        /// Example to show the the AssertIsNotNullAndMatchesRegex validation on a string.
        /// </summary>
        /// <param name="stringParameter">A string. Must not be <see langword="null"/>, at least 5 long and consist of only alphanumerical characters.</param>
        /// <exception cref="ArgumentNullException"><paramref name="stringParameter"/> is <see langword="null"/></exception>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="stringParameter"/> is not at least 5 characters long or does not consist of only alphanumerical characters.</exception>
        public void StringNotNotNullAndMatchesRegexExample([NotNull] string stringParameter)
        {
            // Validate the parameter.
            stringParameter.AssertIsNotNullAndMatchesRegex(@"^[\w]{5,}\z", "stringParameter", "The string \"{0}\" does consist of only alphanumerical characters or it is less than 5 characters long.", stringParameter);

            // Now we know the string matches our expectations, we can do something 
            // useful with it.
        }

        #endregion

    }
}
