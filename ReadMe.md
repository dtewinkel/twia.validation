﻿# Parameter Validation 

This project provides parameter validation for methods and properties. The validation provided is targeted at catching developer errors, related to bad values in input parameters that, during with correct parameter use of the method or property never should happen. The validation is not targeted at catching expected bad values in input of a parameter, for instance because the parameter is the result of user input.

A failed validation will result in an exception of type ``System.ArgumentException``, ``System.ArgumentNullException`` or ``System.ArgumentOutOfRangeException``.

# Usage
The following example shows the use of the ``AssertIsNotNullOrEmpty`` validation on parameter ``param1``. It also shows the combination with the [ReSharper][] ``NotNull`` and ``ContractAnnotation`` attributes.

```cs
    [ContractAnnotation("param1:null => void")]
    public void foo([NotNull] string param1)
    {
        param1.AssertIsNotNullOrEmpty("param1");
        
        ...
    }
```

Validations for _strings_ are:
* ``AssertIsNotNullOrEmpty``
* ``AssertIsNotNullAndMatchesRegex``

Validations for _reference types_ are:
* ``AssertIsNotNull``
* ``AssertIsNotNullAndMeetsCondition``

Validations for _any type_ are:
* ``AssertMeetsCondition``

For a full description of the provided validations, please see the HTML help file.

## Get the package

### NuGet

From [NuGet][]: To get the assemblies, get package with ID Twia.Validation from [NuGet][] or get the latest prerelease from [MyGet twia_dev](https://www.myget.org/F/twia_dev/) (https://www.myget.org/F/twia_dev/) stream. 

The NuGet package includes the HTML help file in the doc directory of the package folder.

### Download
The released versions can be downloaded as a zip file. The zip file includes the assemblies, the xml doc file and the HTML help documentation. 

The zip files of released versions can be downloaded here: [Twia.Validation downloads][].

### Source code

Get the latest sources from Bitbucket: [twia.validation sources](https://bitbucket.org/dtewinkel/twia.validation), or as a zip file from [Twia.Validation downloads][].

## Validate the code



# Development

## Source Control

The (original) sources for this project are stored in a [GIT][] repository on [Bitbucket][]: [twia.validation](https://bitbucket.org/dtewinkel/twia.validation).
 
## Software
This project is built using:
 
* Visual Studio 2015.
* [NuGet extension](http://visualstudiogallery.msdn.microsoft.com/27077b70-9dad-4c64-adcf-c7cf6bc9970c).
* [NuBuild extension](http://visualstudiogallery.msdn.microsoft.com/3efbfdea-7d51-4d45-a954-74a2df51c5d0) to build the NuGet package.
* [Sandcastle Help File Builder](https://github.com/EWSoftware/SHFB) v2015.7.25.0 to create the HTML help file.

Some tools that I found very useful:

* [ReSharper][] by [JetBrains][].
* [MarkdownPad 2](http://markdownpad.com/) to edit this file.
* [Visual Studio Spell Checker](http://vsspellchecker.codeplex.com/), because my spelling is far from perfect.
* [Pandoc](http://johnmacfarlane.net/pandoc/) to convert the ReadMe.md file to html.

# Copyright and License

**Copyright © 2015, Daniël te Winkel, All rights reserved.**

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of Daniël te Winkel nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

[NuGet]: http://www.nuget.org/ "NuGet Package Manager"
[GIT]: http://git-scm.com/ "Git distributed source control system"
[Bitbucket]: https://bitbucket.org/ "Hosting of Git and Mercurial repositories"
[JetBrains]: http://www.jetbrains.com
[ReSharper]: http://www.jetbrains.com/resharper/
[Twia.Validation downloads]: https://bitbucket.org/dtewinkel/twia.validation/downloads "Twia.Validation downloads on Bitbucket"